package ru.roman.sprsecdemo.rest;

import org.springframework.web.bind.annotation.*;
import ru.roman.sprsecdemo.model.Developer;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/api/v1/developers")
public class DeveloperRestControllerV1 {
    private List<Developer> DEVELOPERS
 = Stream.of(
         new Developer(1L, "Ivan", "Ivanov"),
         new Developer(2L, "Qvan", "Qvanov"),
         new Developer(3L, "Avan", "Avanov"),
         new Developer(4L, "Avan", "Avanov"),
         new Developer(5L, "Zvan", "Zvanov")

    ).collect(Collectors.toList());


    @GetMapping
    public List<Developer> getAll(){
        return DEVELOPERS;
    }
    @GetMapping("/{id}")
    public Developer getById(
            @PathVariable Long id){
        return DEVELOPERS.stream()
                .filter(e -> e.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    @PostMapping
    public Developer create(@RequestBody Developer developer){
        this.DEVELOPERS.add(developer);
        return developer;
    }

    @DeleteMapping("/delete/{id}")
    public void deleteById(Long id){
        this.DEVELOPERS.removeIf(developer -> developer.getId().equals(id));
    }
}
